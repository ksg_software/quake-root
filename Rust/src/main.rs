fn quake_root(mut x: f32) -> f32 {
    let xhalf = 0.5 * x;
    let mut i = x.to_bits();

    i = 0x5f37_59df - (i >> 1);
    x = f32::from_bits(i);

    x * (1.5 - xhalf * x * x)
}

fn main() {
    let v: f32 = 0.15625;
    let x = quake_root(v);

    println!("square root of {v} = {}", 1.0 / x);
    println!("inverse square root of {v} = {x}");
}
